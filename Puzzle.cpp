// Cameron Smick
// February 16, 2016
// interface for Puzzle class

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
using namespace std;

// structs that allow me to treat the templated class slightly differently depending on the data type
template <typename T>
struct TypeInt{
	static const bool type = false;
};

template <>
struct TypeInt<int> {
	static const bool type = true;
};

// class definition
template <typename T>
class Puzzle {

	public:
		Puzzle(string fileName = "sudoku.txt"); // constructor
		void printPuzzle() const; // displays the puzzle in its current state to the screen
		void play(); // allows the user to play the game
		void solve(); // implements algorithms to solve the puzzle
	private:
		vector<vector<T> > puzzle; // 2D puzzle vector
		int isAConflict(int, int, int); // returns 1 if there is a conflict in a row, column, or box
		void placeNumber(int, int, int); // places a number onto the puzzle board
		int singlePossibility(vector<vector<vector<int> > > &puz); // implementation of single possibility algorithm
		int singleton(vector<vector<vector<int> > > &puz); // implementation of the singleton algorithm
		void eliminateNumbers(vector<vector<vector<int> > > &puz, int, int, int); // used in solve() to eliminate possibilities
		int isSolved(); // returns 1 if the puzzle is solved
};

// constructor
template <typename T>
Puzzle<T>::Puzzle(string fileName) {

	ifstream dataFile;
	dataFile.open(fileName.c_str());

	string newLine;

	int i;

	// read in data from file and store it in our 2D vector
	while (getline(dataFile, newLine)) {
		vector<T> puzzleRow;
		for(i = 0; i<newLine.length(); i++) {
			if(newLine[i]!=' ') {
				if(TypeInt<T>::type) {
					puzzleRow.push_back(newLine[i] - '0');
				}
				else {
					puzzleRow.push_back(newLine[i]);
				}
			}
		}
		puzzle.push_back(puzzleRow);
	}

	dataFile.close();
}

template <typename T>
void Puzzle<T>::printPuzzle() const {

	int i, j, k;

	// if the puzzle is of type int, it will obtain special formatting
	// otherwise, the puzzle will be printed in a simple grid
	if(TypeInt<T>::type) {
		for(k = 0; k<puzzle.size()+3; k++) {
			cout << "_";
		}
		cout << endl;
		for(i = 0; i<puzzle.size(); i++) {
			cout << "|";
			for(j = 0; j<puzzle[i].size(); j++) {
				cout << puzzle[i][j];
				if(j == 2 | j == 5) {
					cout << "|";
				}
			}
			cout << "|" << endl;
			if(i == 2 | i == 5) {
				for(k = 0; k<puzzle.size()+3; k++) {
					cout << "_";
				}
				cout << "|" << endl;
			}
		}
		for(k=0; k<puzzle.size()+3; k++) {
			cout << "_";
		}
		cout << endl;
	}
	else {
		for(i = 0; i<puzzle.size(); i++) {
			for(j = 0; j<puzzle[i].size(); j++) {
				cout << puzzle[i][j];
			}
			cout << endl;
		}
	}
}

// interactive mode that allows the user to play sudoku
template <typename T>
void Puzzle<T>::play() {

	// runs until the puzzle is solved
	while(!isSolved()) {
		this->printPuzzle();

		int row, col, num;

		do {
			cout << "Please enter the row of the cell that you want to replace: ";
			cin >> row;
		} while(row < 1 || row > puzzle.size());

		row -= 1; // changes from user input to array index

		do {
			cout << "Please enter the column of the cell that you would like to replace: ";
			cin >> col;
		} while(col < 1 || col > puzzle[row].size());

		col -=1; // changes from user input to array index

		do {
			cout << "Please enter the number you would like to place here: ";
			cin >> num;
		} while(num < 1 || num > 9);
		cout << endl;

		// checks to ensure that a certain number can be placed in a certain cell
		if(!isAConflict(row, col, num)) {
			placeNumber(row, col, num); // places number on board
			cout << "The number has been placed in the cell that you specified" << endl;
		}
		else {
			cout << "This number cannot be placed in this location." << endl;
		}
	}

	cout << "Congratulations! You solved the puzzle!" << endl;

	this->printPuzzle();
}

template <typename T>
void Puzzle<T>::solve() {

	vector<vector<vector<int> > > puz; //3D vector of possible values a certain cell can have
	vector<vector<int> > d2; // 2D vectors that will compose the 3D vector
	vector<int> d1; // 1D vectors that will compose the 2D vectors

	int i, j, k;

	// Create 3D vector from empty 2D and 1D vectors
	for(i = 0; i<puzzle.size(); i++) {
		puz.push_back(d2);
		for(j = 0; j<puzzle[i].size(); j++) {
			puz[i].push_back(d1);
			for(k=0; k<puzzle.size(); k++) {
				if(puzzle[i][j]==0 && !isAConflict(i, j, k+1)) {
					puz[i][j].push_back(1); // places a 1 in the third dimension if it is possible to place its corresponding number in the coresponding position on the board
				}
				else {
					puz[i][j].push_back(0); // places a 0 otherwise
				}
			}
		}
	}

	// loops algorithms until the puzzle is solved
	while(!isSolved()) {
		if(!singlePossibility(puz)) {
			singleton(puz);
		}
	}
	this->printPuzzle();
}

template <typename T>
int Puzzle<T>::isAConflict(int row, int col, int num) {

	int i, j;

	//search the column for a conflict
	for(i=0; i<puzzle.size(); i++) {
		if(puzzle[i][col] == num) return 1;
	}

	//check row for conflict
	for(i=0; i<puzzle[row].size(); i++) {
		if(puzzle[row][i] == num) return 1;
	}

	//check box for conflict
	int boxStartRow = 3*(row/3); // this works since integer division always rounds down
	int boxStartCol = 3*(col/3); // this works since integer division always rounds down

	for(i=boxStartRow; i<boxStartRow+3; i++) {
		for(j = boxStartCol; j<boxStartCol+3; j++) {
			if(puzzle[i][j] == num) return 1;
		}
	}

	return 0;
}

template <typename T>
void Puzzle<T>::placeNumber(int row, int col, int num) {

	puzzle[row][col] = num;
}

template <typename T>
int Puzzle<T>::singlePossibility(vector<vector<vector<int> > > &puz) {

	int count, pos, i, j, k;

	// loops through the board and places a number if it is the only number that could go in that specific cell
	// returns 1 if it places a number
	// returns 0 otherwise
	for(i=0; i<puzzle.size(); i++) {
		for(j=0; j<puzzle[i].size(); j++) {
			if(puzzle[i][j]==0) {
				count = 0;
				for(k=0; k<puzzle.size(); k++) {
					if(puz[i][j][k]==1) {
						count += 1;
						pos = k;
					}
				}
				if(count==1) {
					placeNumber(i, j, pos+1);
					eliminateNumbers(puz, i, j, pos);
					return 1;
				}
			}
		}
	}
	return 0;
}

template <typename T>
void Puzzle<T>::eliminateNumbers(vector<vector<vector<int> > > &puz, int row, int col, int pos) {

	int i, j;

	// eliminate possibilities from row
	for(j=0; j<puzzle[row].size(); j++) {
		puz[row][j][pos]=0;
	}

	// eliminate possibilities from column
	for(i=0; i<puzzle.size(); i++) {
		puz[i][col][pos]=0;
	}

	int boxStartRow = 3*(row/3); // this works since integer division always rounds down
	int boxStartCol = 3*(col/3); // this works since integer division always rounds down

	//eliminate possibilities from box
	for(i=boxStartRow; i<boxStartRow+3; i++) {
		for(j = boxStartCol; j<boxStartCol+3; j++) {
			puz[i][j][pos]=0;
		}
	}
}

template <typename T>
int Puzzle<T>::singleton(vector<vector<vector<int> > > &puz) {

	int i, j, k, count, row, col, singleton;

	// check all rows for singleton
	for(i=0; i<puzzle.size(); i++) {
		for(k=0; k<puzzle.size(); k++) {
			count = 0;
			for(j=0; j<puzzle[i].size(); j++) {
				if(puzzle[i][j]==0) {
					if(puz[i][j][k]==1) {
						count += 1;
						row = i;
						col = j;
					}
				}
			}
			if(count==1) {
				singleton=k+1;
				placeNumber(row, col, singleton);
				eliminateNumbers(puz, row, col, k);
				return 1;
			}
		}
	}

	// check all columns for singleton
	for(j=0; j<puzzle.size(); j++) {
		for(k=0; k<puzzle.size(); k++) {
			count = 0;
			for(i=0; i<puzzle.size(); i++) {
				if(puzzle[i][j]==0) {
					if(puz[i][j][k]==1) {
						count += 1;
						row = i;
						col = j;
					}
				}
			}
			if(count==1) {
				singleton=k+1;
				placeNumber(row, col, singleton);
				eliminateNumbers(puz, row, col, k);
				return 1;
			}
		}
	}

	int iStart, jStart;

	// check all boxes for singleton
	for(iStart=0; iStart<puzzle.size(); iStart+=3) {
		for(jStart=0; jStart<puzzle.size(); jStart+=3) {
			for(k=0; k<puzzle.size(); k++) {
				count = 0;
				for(i=iStart; i<iStart+3; i++) {
					for(j=jStart; j<jStart+3; j++) {
						if(puzzle[i][j]==0) {
							if(puz[i][j][k]==1) {
								count += 1;
								row = i;
								col = j;
							}
						}
					}
				}
				if(count==1) {
					singleton=k+1;
					placeNumber(row, col, singleton);
					eliminateNumbers(puz, row, col, k);
					return 1;
				}
			}
		}
	}

	return 0;
}

template <typename T>
int Puzzle<T>::isSolved() {

	int i, j;

	for(i=0; i<puzzle.size(); i++) {
		for(j=0; j<puzzle[i].size(); j++) {
			if(puzzle[i][j] == 0) return 0;
		}
	}

	return 1;
}
