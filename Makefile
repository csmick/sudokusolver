all:	main

main: main.o Puzzle.o
	g++ -g main.o Puzzle.o -o main

%.o: %.cpp
	g++ -g -c -o $@ $<

clean:
	rm -f *.o main
