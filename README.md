Contributors: Cameron Smick
Date created: 2/16/2016

This program is intended to read and solve sudoku puzzles. I have included one file, sudoku.txt, with which you can test the validity of the program's output as
well as see the required formatting for any of your own puzzles that you may like to try it out on.