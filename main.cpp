// Cameron Smick
// February 24, 2016

#include "Puzzle.cpp"

int main() {

	string filename;
	int choice;

	cout << endl << "Please enter the name of the file that contains your puzzle: ";
	cin >> filename;
	Puzzle<int> puz(filename);
	cout << "Please enter '1' if you would like to solve this puzzle yourself." << endl << "Please enter '2' if you would like it solved for you." << endl << "Choice: ";
	cin >> choice;
	while(choice < 1 || choice > 2) {
		cout << "Invalid option." << endl << "Choice: ";
		cin >> choice;
	}
	if(choice==1) puz.play();
	else if(choice==2) puz.solve();
}
